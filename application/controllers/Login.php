<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->library('form_validation');
  }

  function index()
  {
    $data = array();
    $this->load->view('login', $data);
  }

  public function auth()
  {
    $config = array();
    $config[] = validation_set_rules('username', 'Username', 'callback_username_check');
    $config[] = validation_set_rules('password', 'Password', 'required|callback_password_check');

    $response = form_validation($config);

    $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
  }

  public function username_check($str)
  {
    if(empty($str))
    {
      $this->form_validation->set_message('username_check', 'The {field} field is required.');
      return FALSE;
    }

    if ($str == 'test')
    {
      $this->form_validation->set_message('username_check', 'The {field} field can not be the word "test"');
      return FALSE;
    }
    else
    {
      return TRUE;
    }
  }

  public function password_check($str)
  {
    $username = $this->input->post("username");
    $password = $str;

    if(empty($str))
    {
      $this->form_validation->set_message('password_check', 'The {field} field is required.');
      return FALSE;
    }

    if($username !=="admin" && $password !=="admin")
    {
      $this->form_validation->set_message('password_check', 'Username or Password Salah.');
      return FALSE;
    }
  }

}
