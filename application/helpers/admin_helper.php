<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('is_logged_in'))
{
  function is_logged_in()
  {
    $CI =& get_instance();
    $login_username = $CI->session->userdata("login_username");
    $login_password = $CI->session->userdata("login_password");
    $login_name = $CI->session->userdata("login_name");
    $login_level = $CI->session->userdata("login_level");
    $login_role = $CI->session->userdata("login_role");
    $login_datetime = $CI->session->userdata("login_datetime");
  }
}

if(! function_exists('set_url'))
{
  function set_url(...$uri)
  {
    return base_url($uri);
  }
}

if(! function_exists('form_validation'))
{
  function form_validation($config)
  {
    $CI =& get_instance();

    $response["status"] = TRUE;
    $response["validation_result"] = array();

    if($CI->input->post())
    {
      $CI->load->library('form_validation');

      $CI->form_validation->set_rules($config);

      $field_errors = array();
      if ($CI->form_validation->run() == FALSE)
      {
        $errors = $CI->form_validation->error_array();
        foreach($errors as $field => $message)
        {
          $field_errors[] = $field;
          $validation_result[] = array("field" => $field, "status" => FALSE, "message" => $message);
        }
        $response["status"] = FALSE;
      }

      $all_fields = $CI->input->post();
      foreach($all_fields as $field => $val)
      {
        if(!in_array($field, $field_errors))
          $validation_result[] = array("field" => $field, "status" => TRUE, "message" => '');
      }

      $response["validation_result"] = $validation_result;
    }

    return (object)$response;
  }
}

if(! function_exists('validation_set_rules'))
{
  function validation_set_rules($field, $label = "", $rules = "required")
  {
    $response["field"] = $field;
    $response["label"] = $label;
    $response["rules"] = $rules;

    return $response;
  }
}
